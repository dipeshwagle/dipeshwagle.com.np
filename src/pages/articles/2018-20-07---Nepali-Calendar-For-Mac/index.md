---
title: Nepali calendar for Mac that sits in your menubar
date: "2018-07-20T22:40:32.169Z"
layout: post
draft: false
path: "/posts/nepali-calendar-mac/"
category: "Nepali Calendar"
tags:
  - "Mac"
  - "Release"
  - "Electron"
description: "While using Mac it's  convinent to see the current Engilsh date as it sits in your menubar. But whenever I want to see the Nepali date
I have to reach out to my mobile or google it."
---

While using Mac it's  convinent to see the current Engilsh date as it sits in your menubar. But whenever I want to see the Nepali date
I have to reach out to my mobile or google it. So I decided to create a menubar app that allows me to easily glance the current nepali date and see the full calendar when I click the date.

You can see it in action below.

![Nepali_Calendar](./nepali-calendar.gif)

If you have any suggestions or encounter issues don't hesitate to shoot me an email or ping me on twitter.

You can download it from link below.

[Download](./NepaliCalender-0.0.2.dmg)